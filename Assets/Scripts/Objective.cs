﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class Objective : MonoBehaviour
{
    /// <summary>
    /// The complete objective event.
    /// </summary>
    [SerializeField]
    private UnityEvent completeObjective;

    /// <summary>
    /// The timer.
    /// </summary>
    [Range(0, 5)]
    [SerializeField]
    private float timeStamp;

    /// <summary>
    /// This will enable your timer.
    /// </summary>
    private bool enableTimer;

    /// <summary>
    /// mission information for in the hud
    /// </summary>
    [SerializeField]
    private string missionInformation;
    public string MissionInformation
    {
        get
        {
            return missionInformation;
        }
        set
        {
            missionInformation = value;
        }
    }

    /// <summary>
    /// The completed objective bool.
    /// </summary>
    private bool completed = false;
    public bool Completed
    {
        get
        {
            return completed;
        }
        set
        {
            completed = value;
        }
    }

    /// <summary>
    /// The photo cameras
    /// </summary>
    private List<PhotoCamera> photoCameras;

    /// <summary>
    /// Managed the objects.
    /// </summary>
    private ObjectiveManager objectiveManager;
    public ObjectiveManager ObjectiveManager
    {
        get
        {
            return objectiveManager;
        }
        set
        {
            objectiveManager = value;
        }
    }

    private bool isVisible;

    /// <summary>
    /// The header
    /// </summary>
    [Header("Notification of the objectives")]
    [SerializeField]
    private string header;
    public string Header
    {
        get
        {
            return header;
        }
        set
        {
            header = value;
        }
    }

    /// <summary>
    /// The notification
    /// </summary>
    [SerializeField]
    private string notification;
    public string Notification
    {
        get
        {
            return notification;
        }
        set
        {
            notification = value;
        }
    }

    /// <summary>
    /// The photo objectives
    /// </summary>
    [SerializeField]
    private Animal[] photoObjectives;
    public Animal[] PhotoObjectives
    {
        get
        {
            return photoObjectives;
        }
        set
        {
            photoObjectives = value;
        }
    }


    /// <summary>
    /// Starts this instance.
    /// </summary>
    public void Start()
    {
        objectiveManager = GameManager.Instance.GetManager<ObjectiveManager>();
        photoCameras = GameManager.Instance.GetManager<ObjectiveManager>().AllPhotoCameras;
        objectiveManager.Objectives.Add(this);

        enabled = false;
    }

    /// <summary>
    /// Updates this instance.
    /// </summary>
    public void Update()
    {
        //checking if the photocameras are initialized by the game
        if (photoCameras.Count > 0 && objectiveManager.CurrentObjective)
        {
            for (int i = 0; i < photoCameras.Count; i++)
            {

                //Checking if the player has make a photo.
                if (photoCameras[i].MakesPhoto)
                {
                    photoCameras[i].MakesPhoto = false;

                    //checking if the given object is in the photo
                    if (isVisible)
                    {
                        completeObjective.Invoke();
                        break;
                    }
                }
            }
        }
        if (enableTimer)
            Timer();
    }

    /// <summary>
    /// Completes the objective.
    /// </summary>
    public void CompleteObjective()
    {
        Debug.Log("Complete shit");
        if (GameManager.Instance.GetManager<ObjectiveManager>().CheckAnimals(photoObjectives))
        {
            completed = true;
            GameManager.Instance.GetManager<UIManager>().ShowNotification(new string[2] { header, notification });
            enableTimer = true;
        }
    }

    /// <summary>
    /// Called when [became visible].
    /// </summary>
    private void OnBecameVisible()
    {
        isVisible = true;
    }

    /// <summary>
    /// Called when [became invisible].
    /// </summary>
    private void OnBecameInvisible()
    {
        isVisible = false;
    }

    /// <summary>
    /// Timers this instance.
    /// </summary>
    /// <returns>the timestamp</returns>
    private float Timer()
    {
        if (timeStamp > 0)
            return timeStamp -= Time.deltaTime;

        else
        {
            GameManager.Instance.GetManager<UIManager>().ToggleScreen(UIScreens.SCREENS_NotificationScreen);
            enableTimer = false;
            GameManager.Instance.GetManager<ObjectiveManager>().GetObjective();
            return timeStamp;
        }
    }
}

