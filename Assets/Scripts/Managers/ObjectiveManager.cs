﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectiveManager : Manager
{
    /// <summary>
    /// all the objectives
    /// </summary>
    private List<Objective> objectives;
    public List<Objective> Objectives
    {
        get
        {
            if (objectives == null)
                objectives = new List<Objective>();
            return objectives;
        }
        set
        {
            objectives = value;
        }
    }

    /// <summary>
    /// The visible animals
    /// </summary>
    private List<Animal> visibleAnimals;

    /// <summary>
    /// All photo cameras
    /// </summary>
    private List<PhotoCamera> allPhotoCameras;
    public List<PhotoCamera> AllPhotoCameras
    {
        get
        {
            if (allPhotoCameras == null)
                allPhotoCameras = new List<PhotoCamera>();

            return allPhotoCameras;
        }
        set
        {
            allPhotoCameras = value;
        }
    }

    /// <summary>
    /// the current objective the player facing.
    /// </summary>
    private Objective currentObjective;
    public Objective CurrentObjective
    {
        get
        {
            return currentObjective;
        }
        set
        {
            currentObjective = value;
        }
    }

    /// <summary>
    /// Start the instance.
    /// </summary>
    public override void Start()
    {
        base.Start();

        visibleAnimals = new List<Animal>();
    }

    /// <summary>
    /// Adds the animal.
    /// </summary>
    /// <param name="animal">The animal.</param>
    public void AddAnimal(Animal animal)
    {
        visibleAnimals.Add(animal);
    }

    /// <summary>
    /// Removes the animal.
    /// </summary>
    /// <param name="animal">The animal.</param>
    public void RemoveAnimal(Animal animal)
    {
        if (visibleAnimals.Contains(animal))
            visibleAnimals.Remove(animal);
    }

    /// <summary>
    /// Checks the animals.
    /// </summary>
    /// <param name="animals">The animals.</param>
    /// <returns></returns>
    public bool CheckAnimals(Animal[] animals)
    {
        if (animals.Length < 1)
            return false;

        if (visibleAnimals.Count > 0)
        {
            for (int i = 0; i < visibleAnimals.Count; i++)
            {
                if (!visibleAnimals.Contains(animals[i]))
                    return false;
            }
            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// Gets the objective.
    /// </summary>
    /// <returns>current objective</returns>
    public Objective GetObjective()
    {
        List<Objective> specificObjective = new List<Objective>();
        currentObjective = null;

        for (int i = 0; i < objectives.Count; i++)
        {
            if (!objectives[i].Completed)
                specificObjective.Add(objectives[i]);
            else
                objectives[i] = null;
        }

        //checking if there are objectives.
        if (specificObjective.Count > 0)
        {
            Objective nextObjective = specificObjective[Random.Range(0, specificObjective.Count)];
            currentObjective = nextObjective;
            currentObjective.enabled = true;
            return nextObjective;
        }
        return null;
    }
}
