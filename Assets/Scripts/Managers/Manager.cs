﻿using UnityEngine;
using System.Collections;

public class Manager
{
    /// <summary>
	/// Use this for initialization
    /// </summary>
	public virtual void Start () { }
  
    /// <summary>
    /// Update is called once per frame
    /// </summary>
    public virtual void Update () {	}

    /// <summary>
    /// Called in a fixed framerate.
    /// </summary>
    public virtual void FixedUpdate() { }
}
