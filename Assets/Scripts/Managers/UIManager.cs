﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIManager : Manager
{
    /// <summary>
    /// The pause screen
    /// </summary>
    private GameObject[] screens;
    public GameObject[] Screens
    {
        get
        {
            return screens;
        }
        set
        {
            screens = value;
        }
    }

    /// <summary>
    /// The current button the indicator is atm
    /// </summary>
    private int currentButton = 0;

    /// <summary>
    /// All the pauseButtons
    /// </summary>
    [SerializeField]
    private List<Button> buttons = new List<Button>();
    public List<Button> Buttons
    {
        get
        {
            return buttons;
        }
        set
        {
            buttons = value;
        }
    }
    
    /// <summary>
    /// The header of the Objectives notification
    /// </summary>
    [SerializeField]
    private Text header;
    public Text Header
    {
        get
        {
            return header;
        }
        set
        {
            header = value;
        }
    }

    /// <summary>
    /// The notification text of the Objectives
    /// </summary>
    [SerializeField]
    private Text notificationText;
    public Text NotificationText
    {
        get
        {
            return notificationText;
        }
        set
        { 
            notificationText = value;
        }
    }
    
    /// <summary>
    /// The pause indicator
    /// </summary>
    private Image indicator;
    public Image Indicator
    {
        get
        {
            return indicator;
        }
        set
        {
            indicator = value;
        }
    }


    // Use this for initialization
    public override void Start()
    {
        base.Start();
        if(buttons.Count > 0)
            SetPauseButtons();
        
    }

    /// <summary>
    /// Shows the activates and deactivates screens.
    /// </summary>
    public void ToggleScreen(UIScreens type)
    {
        screens[(int)type].SetActive(!screens[(int)type].gameObject.activeInHierarchy);
        if(type == UIScreens.SCREEN_PauseScreen)
            Time.timeScale = (Time.timeScale == 0) ? 1 : 0;
    }

    /// <summary>
    /// Sets PauseScreen buttons.
    /// </summary>
    public void SetPauseButtons()
    {
        Debug.Log("buttons length = " + buttons.Count);
        buttons[0].onClick.AddListener(() => ToggleScreen(UIScreens.SCREEN_PauseScreen));
        buttons[1].onClick.AddListener(() => GameManager.Instance.LoadScene(1));
        buttons[2].onClick.AddListener(() => GameManager.Instance.LoadScene(0));
    }

    /// <summary>
    /// Moves the indicator.
    /// <param name="input">
    /// the certain direction the pause menu has to take.
    /// </param>
    /// </summary>
    public void MoveIndicator(float input)
    {
        if (input > 0.1f)
        {
            if (currentButton > 0)
                currentButton--;
            else
                currentButton = 2;
        }
        if (input < -0.1f)
        {
            if (currentButton < 2)
                currentButton++;
            else
                currentButton = 0;
        }

        indicator.transform.position = new Vector3(indicator.transform.position.x, buttons[currentButton].transform.position.y, indicator.transform.position.z);
    }

    /// <summary>
    /// Selects the button.
    /// </summary>
    public void SelectButton()
    {
        Debug.Log("Select" + buttons[currentButton].gameObject.name);
        buttons[currentButton].onClick.Invoke();
    }

    /// <summary>
    /// Shows the notification.
    /// </summary>
    /// <param name="information">including he header of the objective and the information of the objective.</param>
    public void ShowNotification(string[]  information)
    {
        ToggleScreen(UIScreens.SCREENS_NotificationScreen);

        Transform achievementPanel = screens[(int)UIScreens.SCREENS_NotificationScreen].transform;

        //Show the notification.
        for (int i = 0; i < achievementPanel.childCount; i++)
            achievementPanel.GetChild(i).GetComponent<Text>().text = information[i];
    }

    /// <summary>
    /// Sets the objective hud.
    /// </summary>
    public void SetObjectiveHUD()
    {
        ObjectiveManager objectiveManager = GameManager.Instance.GetManager<ObjectiveManager>();

        if (objectiveManager.CurrentObjective)
            if (!objectiveManager.CurrentObjective.Completed)
                return;
            
        

        Objective nextObjective = objectiveManager.GetObjective();
        if (nextObjective != null)
        {
            header.text = nextObjective.Header;
            NotificationText.text = nextObjective.MissionInformation;
        }
        else
        {
            header.text = string.Empty;
            notificationText.text = string.Empty;
        }
    }
}

/// <summary>
/// all the screens 
/// </summary>
public enum UIScreens
{
    SCREEN_PauseScreen = 0,
    SCREENS_NotificationScreen = 1
}