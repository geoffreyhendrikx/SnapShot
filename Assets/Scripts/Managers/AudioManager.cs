﻿using UnityEngine;
using System.Collections;

public class AudioManager : Manager
{
    /// <summary>
    /// The audio souce
    /// </summary>
    [SerializeField]
    private AudioSource audioSouce;
    /// <summary>
    /// The audio clips
    /// </summary>
    [SerializeField]
    private AudioClip[] audioClips;

    /// <summary>
    /// Starts this instance.
    /// </summary>
    public override void Start()
    {
        base.Start();
        //audioClips = Resources.LoadAll();
    }

    /// <summary>
    /// Plays the sound.
    /// </summary>
    /// <param name="backgroundMusic">The background music.</param>
    public void PlaySound(BackgroundMusic backgroundMusic)
    {
        //audioSouce.Play(audioClips[(int)backgroundMusic]);
        //audioClips[(int)backgroundMusic]
    }
}

/// <summary>
/// All the Background music files in the game will be included in this enum.
/// </summary>
public enum BackgroundMusic
{

}
