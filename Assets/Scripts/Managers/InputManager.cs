﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class InputManager : Manager
{
    /// <summary>
    /// all the PhotoCamera's in the game will be stored in this variable
    /// </summary>
    private PhotoCamera[] photoCamera;

    /// <summary>
    /// The managed the whole UI.
    /// </summary>
    private UIManager uiManager;
    public UIManager UiManager
    {
        get
        {
            return GameManager.Instance.GetManager<UIManager>();
        }
        set
        {
            uiManager = value;
        }
    }

    /// <summary>
    /// [true] disable input.
    /// [false] enable input.
    /// </summary>
    private bool disableInput;
    public bool DisableInput
    {
        get
        {
            return disableInput;
        }
        set
        {
            disableInput = value;
        }
    }

    /// <summary>
    /// The boat Object
    /// </summary>
    private Boat boatActor;
    public Boat BoatActor
    {
        get
        {
            return boatActor;
        }
        set
        {
            boatActor = value;
        }
    }

    /// <summary>
    /// The player
    /// </summary>
    private Player player;
    public Player Player
    {
        get
        {
            return player;
        }
        set
        {
            player = value;
        }
    }

    /// <summary>
    /// The steer to island
    /// </summary>
    private bool steerToIsland;
    public bool SteerToIsland
    {
        get
        {
            return steerToIsland;
        }
        set
        {
            steerToIsland = value;
        }

    }

    /// <summary>
    /// all the variables with the X/Y input value
    /// </summary>
    #region Xbox thumbstick input
    private float xboxInputXRight
    {
        get
        {
            return GamePad.GetState(XInputDotNetPure.PlayerIndex.One).ThumbSticks.Right.X;
        }
        set
        {
            xboxInputXRight = value;
        }

    }
    private float xboxInputYRight
    {
        get
        {
            return GamePad.GetState(XInputDotNetPure.PlayerIndex.One).ThumbSticks.Right.Y;
        }
        set
        {
            xboxInputYRight = value;
        }
    }

    private float xboxInputXLeft
    {
        get
        {
            return GamePad.GetState(XInputDotNetPure.PlayerIndex.One).ThumbSticks.Left.X;

        }
        set
        {
            xboxInputXLeft = value;
        }
    }
    private float xboxInputYLeft
    {
        get
        {
            return GamePad.GetState(XInputDotNetPure.PlayerIndex.One).ThumbSticks.Left.Y;

        }
        set
        {
            xboxInputYLeft = value;
        }
    }
    #endregion

    /// <summary>
    /// for resetting the right trigger.
    /// </summary>
    private bool rightTrigger = false;

    /// <summary>
    /// The active actor
    /// </summary>
    private ControllableActors activeActor = ControllableActors.ACTOR_Boat;

    /// <summary>
    /// The active menus
    /// </summary>
    private DifferentMenus activeMenus = DifferentMenus.None;

    /// <summary>
    /// This is for checking if the actor is switched in the previous frame.
    /// </summary>
    private bool canSwitch;

    /// <summary>
    /// if the game can pause this bool will swith 
    /// </summary>
    private bool canPause;

    /// <summary>
    /// This variable is for resetting the input axis
    /// </summary>
    private bool switchOption;

    /// <summary>
    /// Starts this instance.
    /// </summary>
    public override void Start()
    {
        base.Start();

        //activate when the levelselect is ready.
        switch (GameManager.Instance.LevelNumber)
        {
            case 0:
                activeMenus = DifferentMenus.MENU_MainMenu;
                break;
            case 1:
                activeMenus = DifferentMenus.MENU_LevelSelect;
                break;
            case 2:
                activeMenus = DifferentMenus.None;
                GameManager.Instance.GetManager<UIManager>().SetObjectiveHUD();
                break;
        }

    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    public override void Update()
    {
        #region pseudo Code
        /*
         pseudo code
         #################################################################
         check in what state the actor is (boat, player)
         check if the actor is in the photocamera
         set the actor movement

         disable the actor movement if the actor is in the camera.
         check if the boat is out of bounce
         if the boat is out of bounce set the bool steertoisland on true
         ################################################################
         */
        #endregion

        if (!disableInput || !SteerToIsland)
        {
            //When you are in the boat.
            //controls the boat
            if (activeActor == ControllableActors.ACTOR_Boat && activeMenus == DifferentMenus.None)
            {
                if (!boatActor.InCameraMode)
                {
                    SteerToIsland = boatActor.CheckBoatPosition();
                    boatActor.Controller.SetInputs(GamePad.GetState(PlayerIndex.One).Triggers.Right, xboxInputXLeft);
                }
                PhotoCameraInput(boatActor);
            }

            //Menu Active
            if (activeMenus == DifferentMenus.MENU_PauseMenu)
            {
                if ((xboxInputYLeft < -0.3f || xboxInputYLeft > 0.3f) && !switchOption)
                {
                    GameManager.Instance.GetManager<UIManager>().MoveIndicator(xboxInputYLeft);
                    switchOption = true;
                }
                if (xboxInputYLeft < 0.1f && xboxInputYLeft > -0.1f)
                    switchOption = false;

                if (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed)
                {
                    GameManager.Instance.GetManager<UIManager>().SelectButton();
                    boatActor.MyRigidbody.isKinematic = !boatActor.MyRigidbody.isKinematic;
                    activeMenus = DifferentMenus.None;
                }
            }
        }

        //if the boat is steering to the island
        if (SteerToIsland)
        {
            boatActor.Controller.SetInputs(1f, 0.5f);
            boatActor.CheckBoatPosition();
        }

        ButtonInput();

    }

    /// <summary>
    /// Called in a fixed framerate.
    /// </summary>
    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if (!disableInput || !SteerToIsland)
        {
            //When you are controlling the player so you can walk around
            //moving the player in Fixed update because collider issue
            if (activeActor == ControllableActors.ACTOR_Player && activeMenus == DifferentMenus.None)
            {
                if (!player.InCameraMode)
                {
                    player.MoveActor(xboxInputYLeft, xboxInputXLeft);
                    player.RotateActor(xboxInputYRight, xboxInputXRight);
                }
                PhotoCameraInput(player);
            }
        }

    }

    /// <summary>
    /// Switches the actor who the player controls.
    /// if youre controlling the Player this function will switch that to the boat and vice versa
    /// </summary>
    public void SwitchActor()
    {

        switch (activeActor)
        {
            case ControllableActors.ACTOR_Player:
                if (player.InCameraMode)
                    return;
                activeActor = ControllableActors.ACTOR_Boat;
                break;

            case ControllableActors.ACTOR_Boat:
                if (boatActor.InCameraMode)
                    return;


                activeActor = ControllableActors.ACTOR_Player;
                player.TeleportStartPosition();
                Debug.Log("zero");
                boatActor.MyRigidbody.velocity = Vector3.zero;
                boatActor.transform.eulerAngles = Vector3.zero;
                player.MyRigidbody.velocity = Vector3.zero;
                //BoatActor.MyRigidbody.constraints = RigidbodyConstraints.FreezeAll;
                break;

            default:
                Debug.LogError("Active actor in a strange state: " + activeActor.ToString() + ". Contact Geoffrey Hendrikx if this message appear");
                Debug.Break();
                break;

        }
        canSwitch = true;
        BoatActor.ActorsCamera.enabled = !BoatActor.ActorsCamera.enabled;
        player.ActorsCamera.enabled = !player.ActorsCamera.enabled;
    }

    /// <summary>
    /// input function for only the photocamera.
    /// </summary>
    /// <param name="actor">The actor.</param>
    public void PhotoCameraInput(Actor actor)
    {
        //two the same if statements?

        if (GamePad.GetState(PlayerIndex.One).Triggers.Left > .9f)
        {
            boatActor.MyRigidbody.isKinematic = true;
            if (!actor.InCameraMode)
                actor.PhotoCamera.TriggerCamera();
            actor.InCameraMode = true;
        }

        else
        {
            boatActor.MyRigidbody.isKinematic = false;
            if (actor.InCameraMode)
                actor.PhotoCamera.TriggerCamera();
            actor.InCameraMode = false;
        }

        if (actor.InCameraMode)
            actor.PhotoCamera.RotateCamera(xboxInputYLeft, xboxInputXLeft);

        //if the camera mode is active
        if (actor.InCameraMode)
        {
            if (GamePad.GetState(PlayerIndex.One).Triggers.Right > .9f && !rightTrigger)
            {
                actor.StartCoroutine(actor.PhotoCamera.TakePhoto());
                rightTrigger = true;
            }
            if (GamePad.GetState(PlayerIndex.One).Triggers.Right < .1f && rightTrigger)
                rightTrigger = false;

            actor.PhotoCamera.RotateCamera(xboxInputYLeft, xboxInputXLeft);
        }
    }

    /// <summary>
    /// Input of all the buttons of the .
    /// </summary>
    public void ButtonInput()
    {
        //toggle pause screena 
        if (GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed && canPause)
        {
            GameManager.Instance.GetManager<UIManager>().ToggleScreen(UIScreens.SCREEN_PauseScreen);
            boatActor.MyRigidbody.isKinematic = !boatActor.MyRigidbody.isKinematic;
            activeMenus = (activeMenus == DifferentMenus.None) ? DifferentMenus.MENU_PauseMenu : DifferentMenus.None;
            canPause = false;
            boatActor.Hud.gameObject.SetActive(!boatActor.Hud.gameObject.activeInHierarchy);
        }

        //reset start button
        if (GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Released && !canPause)
            canPause = true;

        //Switch the acter (playable character) to player or boat
        if (GamePad.GetState(PlayerIndex.One).Buttons.Y == ButtonState.Pressed && !canSwitch)
            SwitchActor();

        //get a new objective
        if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed && activeMenus == DifferentMenus.None)
            GameManager.Instance.GetManager<UIManager>().SetObjectiveHUD();

        //reset Y button
        if (GamePad.GetState(PlayerIndex.One).Buttons.Y == ButtonState.Released)
            canSwitch = false;
    }

}

/// <summary>
/// The actor you control. 
/// </summary>
public enum ControllableActors
{
    ACTOR_Player,
    ACTOR_Boat
}

/// <summary>
/// Different menu states
/// </summary>
public enum DifferentMenus
{
    None,
    MENU_PauseMenu,
    MENU_LevelSelect,
    MENU_MainMenu
}