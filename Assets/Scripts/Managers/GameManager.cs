﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    /// <summary>
    /// The instance of the GameManager
    /// </summary>
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<GameManager>();
            if (instance == null)
                Debug.LogError("Game could not be found, please add one.");

            return instance;
        }
        set
        {
            instance = value;
        }
    }

    /// <summary>
    /// All the UI screens
    /// </summary>
    [SerializeField]
    private GameObject[] UiScreens;
    /// <summary>
    /// The pause buttons
    /// </summary>
    /// 
    [SerializeField]
    private List<Button> SceneButtons;

    /// <summary>
    /// The pause menu indicator
    /// </summary>
    [SerializeField]
    private Image indicator;

    /// <summary>
    /// The UI header
    /// </summary>
    [SerializeField]
    private Text header;

    /// <summary>
    /// The content
    /// </summary>
    [SerializeField]
    private Text Content;

    /// <summary>
    /// The level number
    /// </summary>
    private int levelNumber;
    public int LevelNumber
    {
        get
        {
            return SceneManager.GetActiveScene().buildIndex;
        }
    }

    /// <summary>
    /// all the managers.
    /// this will be used for updating the managers
    /// </summary>
    [SerializeField]
    private Manager[] managers;
    [SerializeField]
    private AudioSource audioSource;

    /// <summary>
    /// Awakes this instance.
    /// </summary>
    public void Awake()
    {
        //Initialize managers
        managers = new Manager[5]
        {
            new ActorManager(),
            new AudioManager(),
            new ObjectiveManager(),
            new InputManager(),
            new UIManager()
        };

        #region UIManager set variables
        UIManager uiManager = GetManager<UIManager>();
        if (indicator)
        {
            uiManager.Screens = UiScreens;
            uiManager.Buttons = SceneButtons;
            uiManager.Indicator = indicator;
            uiManager.Header = header;
            uiManager.NotificationText = Content;
        }
        #endregion
        
        if(audioSource)
            audioSource.Play();
    }

    /// <summary>
    /// Starts all the managers.
    /// </summary>
    public void Start()
    {
        for (int i = 0; i < managers.Length; i++)
            managers[i].Start();
    }

    /// <summary>
    /// Updates all the managers.
    /// </summary>
    public void Update()
    {
        for (int i = 0; i < managers.Length; i++)
            managers[i].Update();
    }

    /// <summary>
    /// Called in a fixed framerate.
    /// </summary>
    public void FixedUpdate()
    {
        for (int i = 0; i < managers.Length; i++)
            managers[i].FixedUpdate();
    }

    /// <summary>
    /// Load the 
    /// </summary>
    /// <param name="levelIndex"></param>
    public void LoadScene(int levelIndex)
    {
        SceneManager.LoadScene(levelIndex);
    }

    /// <summary>
    /// Exits the game.
    /// </summary>
    public void ExitGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// Get the certain manager.
    /// </summary>
    /// <typeparam name="T">Certain manager</typeparam>
    /// <returns>T</returns>
    public T GetManager<T>() where T : Manager
    {
        //looped through each managers
        for (int i = 0; i < managers.Length; i++)
            if (managers[i].GetType() == typeof(T))
                return (T)managers[i];

        return default(T);

    }
}
