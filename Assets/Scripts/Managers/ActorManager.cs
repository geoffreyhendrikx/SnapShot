﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActorManager : Manager
{
    /// <summary>
    /// The animals who are visible
    /// </summary>
    private List<Actor> visibleAnimals = new List<Actor>();
    public List<Actor> VisibleAnimals
    {
        get
        {
            return visibleAnimals;
        }
        set
        {
            visibleAnimals = value;
        }
    }

    /// <summary>
    /// Adds the actor to the visibleAnimals list.
    /// </summary>
    /// <param name="animal">The animal.</param>
    public void AddActor(Actor animal)
    {
        if (animal.GetType() == typeof(Animal))
            visibleAnimals.Add(animal);
    }

    /// <summary>
    /// Removes the actor from the visibleAnimals room.
    /// </summary>
    /// <param name="animal">The animal.</param>
    public void RemoveActor(Actor animal)
    {
        for (int i = 0; i < visibleAnimals.Count; i++)
        {
            if(visibleAnimals[i] == animal)
                visibleAnimals.RemoveAt(i);
        }
    }

}
