﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;

[System.Serializable]
public class PhotoCamera
{
    [Header("PhotoCamera Properties")]
    /// <summary>
    /// The camera overlay
    /// </summary>
    [SerializeField]
    private Image cameraOverlay;

    /// <summary>
    ///the games hud
    /// </summary>
    [SerializeField]
    private GameObject Hud;

    /// <summary>
    /// the image flash
    /// </summary>
    [SerializeField]
    private Image flash;

    /// <summary>
    /// The this camera
    /// </summary>
    [SerializeField]
    private Camera thisCamera;
    public Camera ThisCamera
    {
        get
        {
            return thisCamera;
        }
        set
        {
            thisCamera = value;
        }
    }

    /// <summary>
    /// The view angle of the camera
    /// </summary>
    [SerializeField]
    private float viewAngle;

    /// <summary>
    /// The camera type
    /// </summary>
    [SerializeField]
    private CameraType cameraType;
    public CameraType CameraType
    {
        get
        {
            return cameraType;
        }
        set
        {
            cameraType = value;
        }
    }

    [SerializeField]
    private Animator myAnimator;

    /// <summary>
    /// for setting a max and a min on the rotation.
    /// </summary>
    private float cameraRotation;

    [SerializeField]
    private Camera cacheCamera;

    /// <summary>
    /// when this photocamera makes the photo 
    /// </summary>
    private bool makesPhoto;
    public bool MakesPhoto
    {
        get
        {
            return makesPhoto;
        }
        set
        {
            makesPhoto = value;
        }
    }

    /// <summary>
    /// the object manager
    /// </summary>
    private ObjectiveManager objectiveManager
    {
        get
        {
            return GameManager.Instance.GetManager<ObjectiveManager>();
        }
    }

    /// <summary>
    /// Takes and saves the photo.
    /// </summary>
    public IEnumerator TakePhoto()
    {
        if (objectiveManager.CurrentObjective)
            makesPhoto = true;
        else
            makesPhoto = false;
        if (flash.color.a > 1)
        {
            Debug.Log(flash.color.a);
            yield return null;
        }
        else
        {
            cameraOverlay.gameObject.SetActive(false);
            yield return new WaitForEndOfFrame();
            if (!cacheCamera.enabled)
                Application.CaptureScreenshot(FileName());
            yield return new WaitForEndOfFrame();
            cameraOverlay.gameObject.SetActive(true);
            thisCamera.GetComponent<Animator>().SetTrigger("Flash");
        }

    }

    /// <summary>
    /// The name of the file.
    /// </summary>
    private string FileName()
    {
        string date = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
        string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + "\\SnapShot!";

        if (!Directory.Exists(folderPath))
            Directory.CreateDirectory(folderPath);

        string fileName = string.Format("{0}\\SnapShot - {1}.png", folderPath, date);

        return fileName;
    }

    /// <summary>
    /// Moves the camera.
    /// </summary>
    public void RotateCamera(float vertical, float horizontal)
    {
        Quaternion newEulerAngle = Quaternion.Euler(thisCamera.transform.rotation.eulerAngles.x - vertical * Time.deltaTime * cameraRotation,
                    thisCamera.transform.rotation.eulerAngles.y + horizontal * Time.deltaTime * cameraRotation,
                    thisCamera.transform.rotation.eulerAngles.z);

        if (CameraType == CameraType.Camera_PhotoCamera)
            thisCamera.transform.rotation = newEulerAngle;
        else if (CameraType == CameraType.CAMERA_UnderwaterCamera)
        {
            thisCamera.transform.eulerAngles = new Vector3(thisCamera.transform.rotation.eulerAngles.x - vertical * Time.deltaTime * cameraRotation,
                thisCamera.transform.rotation.eulerAngles.y + horizontal * Time.deltaTime * cameraRotation, thisCamera.transform.rotation.eulerAngles.z);
        }
    }

    /// <summary>
    /// Removes the photo.
    /// </summary>
    private void RemovePhoto()
    {

    }

    /// <summary>
    /// sets the camera on or off in Hierarchy.
    /// </summary>
    public void TriggerCamera()
    {
        if (!cacheCamera)
            cacheCamera = Camera.main;

        Hud.SetActive(!Hud.gameObject.activeInHierarchy);

        //Move camera but check on the values where the camera can move towards
        thisCamera.enabled = !thisCamera.enabled;

        cacheCamera.enabled = !cacheCamera.enabled;

    }
}

/// <summary>
/// Different camera types
/// </summary>
public enum CameraType
{
    CAMERA_UnderwaterCamera,
    Camera_PhotoCamera
}