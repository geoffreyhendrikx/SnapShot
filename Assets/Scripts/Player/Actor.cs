﻿using UnityEngine;
using System.Collections;

public class Actor : MonoBehaviour
{
    /// <summary>
    /// Rigidbody of this actor.
    /// </summary>
    [Header("Actor Properties")]
    [SerializeField]
    private Rigidbody myRigidbody;
    public Rigidbody MyRigidbody
    {
        get
        {
            return myRigidbody;
        }
        set
        {
            myRigidbody = value;
        }
    }

    /// <summary>
    /// The speed of this actor.
    /// </summary>
    [SerializeField]
    private float speed;
    public float Speed
    {
        get
        {
            return speed;
        }
        set
        {
            speed = value;
        }
    }

    /// <summary>
    /// the animator of the actor
    /// </summary>
    [SerializeField]
    private Animator myAnimator;
    public Animator MyAnimator
    {
        get
        {
            return myAnimator;
        }
        set
        {
            myAnimator = value;
        }
    }

    /// <summary>
    /// The input manager
    /// </summary>
    private InputManager inputManager;
    public InputManager InputManager
    {
        get
        {
            return GameManager.Instance.GetManager<InputManager>();
        }
        set
        {
            inputManager = value;
        }
    }

    /// <summary>
    /// Indicates if the player is in the camera mode
    /// </summary>
    private bool inCameraMode;
    public bool InCameraMode
    {
        get
        {
            return inCameraMode;
        }
        set
        {
            inCameraMode = value;
        }
    }

    /// <summary>
    /// The actors camera
    /// </summary>
    [SerializeField]
    private Camera actorsCamera;
    public Camera ActorsCamera
    {
        get
        {
            return actorsCamera;
        }
        set
        {
            actorsCamera = value;
        }
    }

    /// <summary>
    /// The photo camera of this actor
    /// </summary>
    [SerializeField]
    private PhotoCamera photoCamera;
    public PhotoCamera PhotoCamera
    {
        get
        {
            return photoCamera;
        }
        set
        {
            photoCamera = value;
        }
    }

    /// <summary>
    /// The hud
    /// </summary>
    [SerializeField]
    private GameObject hud;
    public GameObject Hud
    {
        get
        {
            return hud;
        }
        set
        {
            hud = value;
        }
    }

    public virtual void Start()
    {
        GameManager.Instance.GetManager<ObjectiveManager>().AllPhotoCameras.Add(PhotoCamera);
    }

    public virtual void Update() { }
}
