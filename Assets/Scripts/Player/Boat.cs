﻿using UnityEngine;
using System.Collections;

public class Boat : Actor
{
    /// <summary>
    /// The brake speed of the boat
    /// </summary>
    [Header("Boat Extra Properties")]
    [SerializeField]
    private float brakeSpeed;


    #region the out of bounds borders
    /// <summary>
    /// The right under border
    /// </summary>
    [SerializeField]
    private Transform rightUnderBorder;
    /// <summary>
    /// The left top border
    /// </summary>
    [SerializeField]
    private Transform leftTopBorder;
    #endregion

    /// <summary>
    /// This controller is controlling the movement of the boat
    /// </summary>
    [SerializeField]
    private BoatController controller;
    public BoatController Controller
    {
        get
        {
            return controller;
        }
        set
        {
            controller = value;
        }
    }

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        InputManager.BoatActor = this;
    }

    /// <summary>
    /// Checks the boat position if it's out of bounds.
    /// if the boat is out of bounds it will return true.
    /// </summary>
    public bool CheckBoatPosition()
    {
        Vector3 boatPosition = transform.position;

        // checking the positions of the boat.
        if (boatPosition.x > rightUnderBorder.transform.position.x)
            return true;
        if(boatPosition.z < rightUnderBorder.transform.position.z)
            return true;
        if (boatPosition.z > leftTopBorder.transform.position.z)
            return true;
        if (boatPosition.x < leftTopBorder.transform.position.x)
            return true;

        return false;

    }
}
