﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : Actor
{
    [Header("Player Properties")]
    ///<summary>
    /// The first eulerangle of the player. (Camera Rotation)
    /// </summary>
    private Vector3 firstEulerAngle;

    /// <summary>
    /// The maximum angle of the Players camera.
    /// </summary>
    [Range(200, 300)]
    [SerializeField]
    private int maxAngle;

    /// <summary>
    /// The minimum angle of the Players camera.
    /// </summary>
    [Range(45, 80)]
    [SerializeField]
    private int minAngle;

    /// <summary>
    /// The speed of lookrotation of the player.
    /// </summary>
    [Range(50, 200)]
    [SerializeField]
    private float cameraRotationSpeed;

    [SerializeField]
    private Transform startPosition;

    /// <summary>
    /// Use this for initialization
    /// </summary>
    public override void Start()
    {
        base.Start();

        InputManager.Player = this;
        firstEulerAngle = transform.rotation.eulerAngles;
        this.gameObject.SetActive(false);
    }

    /// <summary>
    /// Moves the actor.
    /// </summary>
    /// <param name="Speed">The speed.</param>
    public void MoveActor(float inputY, float inputX)
    {
        Vector3 forwardMovement = (ActorsCamera.transform.forward * inputY * Speed * Time.deltaTime);
        Vector3 sidewaysMovement = (ActorsCamera.transform.right * inputX * Speed * Time.deltaTime);

        MyRigidbody.MovePosition(gameObject.transform.position + (forwardMovement) + (sidewaysMovement));
    }

    /// <summary>
    /// Teleports the start position.
    /// </summary>
    public void TeleportStartPosition()
    {
        this.gameObject.SetActive(true);
        this.gameObject.transform.position = startPosition.position;
    }

    /// <summary>
    /// Rotates the actor.
    /// </summary>
    /// <param name="vertical">The vertical.</param>
    /// <param name="horizontal">The horizontal.</param>
    public void RotateActor(float vertical, float horizontal)
    {
        /*
        Checking if the player isn't outside his range.
        */

       if (transform.eulerAngles.x - vertical < minAngle || transform.eulerAngles.x - vertical > maxAngle)
        ActorsCamera.transform.eulerAngles =
            new Vector3(ActorsCamera.transform.rotation.eulerAngles.x - vertical * Time.deltaTime * cameraRotationSpeed,
                        ActorsCamera.transform.rotation.eulerAngles.y + horizontal * Time.deltaTime * cameraRotationSpeed,
                        ActorsCamera.transform.rotation.eulerAngles.z);
        PhotoCamera.ThisCamera.gameObject.transform.eulerAngles = ActorsCamera.transform.eulerAngles;
    }
}
