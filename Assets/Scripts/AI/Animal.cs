﻿using UnityEngine;
using System.Collections;

public class Animal : Actor
{
    [Header("Animal Properties", order = 1)]
    /// <summary>
    /// My steering behaviours
    /// </summary>
    /// TODO change to protected
    [SerializeField]
    private SteeringBehaviours mySteeringBehaviour;
    public SteeringBehaviours MySteeringBehaviour
    {
        get
        {
            return mySteeringBehaviour;
        }
        set
        {
            mySteeringBehaviour = value;
        }
    }

    /// <summary>
    /// The cetain path the AI will have to follow
    /// </summary>
    [SerializeField]
    private GameObject certainPath;
    public GameObject CertainPath
    {
        get
        {
            return certainPath;
        }
        set
        {
            certainPath = value;
        }
    }

    /// <summary>
    /// The current velocity
    /// </summary>
    private Vector3 currentVelocity;

    /// <summary>
    /// The maximum speed
    /// </summary>
    private float maxSpeed = 5;
    public float MaxSpeed
    {
        get
        {
            return maxSpeed;
        }
        set
        {
            maxSpeed = value;
        }
    }

    /// <summary>
    /// Give the trigger animation name.
    /// </summary>
    [SerializeField]
    private string animationName;

    /// <summary>
    /// Starts this instance.
    /// </summary>
    public override void Start()
    {
        currentVelocity = MyRigidbody.velocity;
        GameManager.Instance.GetManager<ActorManager>().AddActor(this);
        MySteeringBehaviour.ThisAnimal = this;
        MySteeringBehaviour.InitializeEvent();
    }

    /// <summary>
    /// Updates this instance.
    /// </summary>
    public override void Update()
    {
        Vector3 newForce = MySteeringBehaviour.TotalForce();

        if(newForce.sqrMagnitude > (MaxSpeed * MaxSpeed))
            newForce = newForce.normalized * MaxSpeed;

        newForce = newForce * Time.deltaTime;
        currentVelocity += newForce;
        MyRigidbody.velocity = currentVelocity;

        //will look at the direction he's moving
        transform.LookAt(transform.position + MySteeringBehaviour.PathFollow().normalized);
    }

    /// <summary>
    /// Animal is visible
    /// </summary>
    public void OnBecameVisible()
    {
        if (GameManager.Instance)
            GameManager.Instance.GetManager<ObjectiveManager>().AddAnimal(this);
    }

    /// <summary>
    /// Animal isn't in the render view
    /// </summary>
    public void OnBecameInvisible()
    {
        if(GameManager.Instance)
            GameManager.Instance.GetManager<ObjectiveManager>().RemoveAnimal(this);
    }

    /// <summary>
    /// Plays the animation.
    /// </summary>
    public void PlayAnimation()
    {
        MyAnimator.SetTrigger(animationName);
    }
}
