﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// All the methodes in this class will return a force where the specific actor needs to go
/// </summary>
[System.Serializable]
public class SteeringBehaviours
{
    //These bools will be used for setting the behaviours of the specific AI
    #region Behaviours

    [SerializeField]
    private bool isCohesion;
    public bool IsCohesion
    {
        get
        {
            return isCohesion;
        }
        set
        {
            isCohesion = value;
        }
    }

    [SerializeField]
    private bool isSeperation;
    public bool IsSeperation
    {
        get
        {
            return isSeperation;
        }
        set
        {
            isSeperation = value;
        }
    }

    [SerializeField]
    private bool isAlignment;
    public bool IsAlignment
    {
        get
        {
            return isAlignment;
        }
        set
        {
            isAlignment = value;
        }
    }

    [SerializeField]
    private bool isPathFollow;
    public bool IsPathFollow
    {
        get
        {
            return isPathFollow;
        }
        set
        {
            isPathFollow = value;
        }
    }

    [SerializeField]
    private bool isFlocking;
    public bool IsFlocking
    {
        get
        {
            return isFlocking;
        }
        set
        {
            isFlocking = value;
        }
    }
    #endregion
    
    private delegate Vector3 CombinedForcesEvent();
    private CombinedForcesEvent combinedForcesEvent;

    /// <summary>
    /// The max neighbor distance
    /// </summary>
    [SerializeField]
    [Range(0,10)]
    private float neighborDistance = 3.5f;
    
    /// <summary>
    /// The current point used for path following
    /// </summary>
    private static int currentPoint = 0;

    /// <summary>
    /// the animal with the specific steeringbehaviour
    /// </summary>
    private Animal thisAnimal;
    public Animal ThisAnimal
    {
        get
        {
            return thisAnimal;
        }
        set
        {
            thisAnimal = value;
        }
    }

    /// <summary>
    /// Get's the instance of the ActorManager;
    /// </summary>
    private ActorManager actorManager
    {
        get
        {
            return GameManager.Instance.GetManager<ActorManager>();
        }
    }

    /// <summary>
    /// All the AI's
    /// </summary>
    private List<Actor> allAnimals
    {
        get
        {
            return actorManager.VisibleAnimals;
        }
    }

    /// <summary>
    /// Initializes all the events.
    /// </summary>
    public void InitializeEvent()
    {
        if (isCohesion && IsFlocking)
            combinedForcesEvent += Cohesion;

        if (isSeperation || IsFlocking)
            combinedForcesEvent += Seperate;

        if (isAlignment || IsFlocking)
            combinedForcesEvent += Alignment;

        if (isPathFollow && thisAnimal.CertainPath)
            combinedForcesEvent += PathFollow;

    }

    /// <summary>
    /// get's all the AI's and gonna calculate the average of it.
    /// give the force to the avarage position
    /// </summary>
    /// <returns>vector avarage of all AI's</returns>
    public Vector3 Cohesion()
    {
        Vector3 force = Vector3.zero;

        int count = 0;

        for (int i = 0; i < allAnimals.Count; i++)
        {
            if (allAnimals[i] != thisAnimal)
            {
                float distance = Vector3.Distance(thisAnimal.transform.position, allAnimals[i].transform.position);
                if (distance > 0 && distance < neighborDistance)
                {
                    force += allAnimals[i].transform.position;
                    count++;
                }
            }
        }
        
        if (count > 0)
        {
            force /= count;
            return Seek(force);
        }
        else
            return Vector3.zero;
    }

    /// <summary>
    /// Seperating all the animals.
    /// </summary>
    /// <returns>cohesion vector but reversed</returns>
    public Vector3 Seperate()
    {
        Vector3 force = Vector3.zero;
        int neighbors = 0;

        for (int i = 0; i < allAnimals.Count; i++)
        {
            if (allAnimals[i] != thisAnimal && Vector3.Distance(allAnimals[i].transform.position, thisAnimal.transform.position) <= neighborDistance)
            {
                force.x = allAnimals[i].transform.position.x - thisAnimal.transform.position.x;
                force.y = allAnimals[i].transform.position.y - thisAnimal.transform.position.y;
                force.z = allAnimals[i].transform.position.z - thisAnimal.transform.position.z;

                neighbors++;
            }
        }
        if (neighbors > 0)
        {
            force.x /= neighbors;
            force.z /= neighbors;

            force *= -1;
        }
        force.Normalize();
        force *= neighborDistance;

        return force;
    }

    /// <summary>
    /// Aling all the AI to the same direction
    /// </summary>
    /// <returns>Heading direction</returns>
    public Vector3 Alignment()
    {
        Vector3 force = Vector3.zero;

        int count = 0;

        for (int i = 0; i < allAnimals.Count; i++)
        {
            float distance = Vector3.Distance(thisAnimal.transform.position, allAnimals[i].transform.position);
            if (distance > 0 && distance < neighborDistance)
            {
                force += allAnimals[i].MyRigidbody.velocity;
                count++;
            }
        }
        if (count > 0)
        {
            force /= count;
            force.Normalize();
            force *= thisAnimal.Speed;
            Vector3 steer = force - thisAnimal.MyRigidbody.velocity;
            if (steer.sqrMagnitude > (thisAnimal.MaxSpeed * thisAnimal.MaxSpeed))
                steer = steer.normalized * thisAnimal.MaxSpeed;


            return steer;
        }
        else
            return Vector3.zero;
    }

    /// <summary>
    /// This function will make sure the AI is on the right path
    /// </summary>
    /// <param name="path">the certain path the AI has</param>
    /// <returns>seeking the next point direction</returns>
    public Vector3 PathFollow()
    {
        GameObject path = thisAnimal.CertainPath;
        Vector3 seekPosition = path.transform.GetChild(currentPoint).transform.position;


        if (Vector3.Distance(thisAnimal.transform.position, seekPosition) < neighborDistance)
        {
            currentPoint++;

            if (currentPoint == path.transform.childCount)
                currentPoint = 0;

            seekPosition = path.transform.GetChild(currentPoint).transform.position;
        }

        return Seek(seekPosition);

    }

    /// <summary>
    /// seek the next point
    /// </summary>
    /// <returns>the vector which the AI was seeking</returns>
    public Vector3 Seek(Vector3 target)
    {
        Vector3 desiredVelocity = Vector3.Normalize(target - thisAnimal.transform.position) * thisAnimal.Speed;
        return (desiredVelocity - thisAnimal.MyRigidbody.velocity);
    }

    /// <summary>
    /// this function will make sure the AI will fly or swim in a group (flocking behaviour)
    /// </summary>
    /// <returns>the certain force</returns>
    public Vector3 TotalForce()
    {
        return combinedForcesEvent.Invoke();
    }
}
